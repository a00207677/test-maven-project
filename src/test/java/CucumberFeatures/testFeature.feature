
@browser
Feature: Failure to log in - test

  Scenario Outline: Login failure
    Given I am on the login page
		When I enter my email as <email>
    And I enter my password as <password>
    And I click the sign in button
    Then I should see the following message : <message>

    Examples: 
      | email  		 | password | message  				 |
      | 123@321.ie |     test | There is 1 error |
