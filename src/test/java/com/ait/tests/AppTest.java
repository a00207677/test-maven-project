package com.ait.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.testng.annotations.Test;

public class AppTest{
    boolean testBool = true;
    
    @Test
    public void testAppPass() {
        assertTrue( testBool );
    }
    
    @Test
    public void testAppFail() {
        assertFalse( !testBool );
    }
}
