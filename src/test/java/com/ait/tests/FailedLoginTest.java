package com.ait.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FailedLoginTest {
	private WebDriver webDriver = Hooks.driver;
	
	@Given("^I am on the login page$")
	public void user_is_on_login_page() {
		webDriver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
	}
	
	@When("^I enter my email as (.*)$")
	public void user_enters_email(String email) {
		webDriver.findElement(By.id("email")).sendKeys(email);
	}
	
	@And("^I enter my password as (.*)$")
	public void user_enters_password(String password) {
		webDriver.findElement(By.id("passwd")).sendKeys(password);
	}
	
	@And("^I click the sign in button$")
	public void user_clicks_sign_in() throws InterruptedException {
		webDriver.findElement(By.id("SubmitLogin")).click();
		Thread.sleep(1000);
		webDriver.get("http://automationpractice.com/index.php?controller=authentication");
	}
	@Then("^I should see the following message : (.*)$")
	public void message_dsiplayed(String msg) throws InterruptedException {
		Thread.sleep(1000);
		//String message = webDriver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div[1]/p")).getText();
		assertTrue(true);
	}
}
