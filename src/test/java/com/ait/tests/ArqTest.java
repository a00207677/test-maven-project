package com.ait.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.jboss.arquillian.core.api.annotation.Inject;
import org.junit.Test;

import pipeline_demo.pipeline_demo.CapsService;

public class ArqTest {
	@Inject
	private CapsService capsService;
	     
	@Test
	public void givenWord_WhenUppercase_ThenLowercase(){
	    assertTrue("capitalize".equals(capsService.getConvertedCaps("CAPITALIZE")));
	    assertEquals("capitalize", capsService.getConvertedCaps("CAPITALIZE"));
	}
}
